#include <gtest/gtest.h>

using namespace std;

#include "../src/ServerTCP.h"

class ServerTest : public ::testing::Test {

    protected:
        ServerTest() {
            //server = new ServerTCP()
        }
        // destructor at the very end
        virtual ~ServerTest() {
            //delete server; server = nullptr;
        };
        // init and destroy at every test
        void SetUp() override {
            server = new ServerTCP( 12345, io );
        }
        //
        virtual void TearDown() override {
            delete server;
            server = nullptr;

        };
    // all accessible
    ServerTCP *server;
    asio::io_context io;
};

TEST_F( ServerTest, serverConstructinoNotRunnig ) {
    EXPECT_NE( server, nullptr );
    EXPECT_EQ( server->isRunning(), false  );
}
