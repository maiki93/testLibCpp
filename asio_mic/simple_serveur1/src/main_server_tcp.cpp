
#include <iostream>

//#include <asio.hpp>
#include "ServerTCP.h"

// Run the server, this function will block when server is running (a way to stop ?)
// One io_context, one_thread
int main()
{
  asio::io_context io_context;

  ServerTCP server( 12345, io_context );
  std::cout << "isRunning: " << server.isRunning()  << std::endl;

  server.start(); // fill first async call wait to read data
  io_context.run();
  std::cout << "end " << std::endl;
}
