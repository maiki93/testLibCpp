#include <iostream>
#include <asio.hpp>

int main(int argc, char *argv[])
{
    // need at least on I/O execution context, must be passed to all core asio classes that provide I/O functionality  as first argument
    asio::io_context io;

    asio::steady_timer timer( io, asio::chrono::seconds(5) );
    std::cout << "creation timer, execute" << std::endl;
    // execute timer
    timer.wait();
    std::cout << "after execution" << std::endl;

	std::cout << "Hello World" << std::endl;
	return 0;
}
