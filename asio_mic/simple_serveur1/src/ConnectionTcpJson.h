#ifndef CONNECTIONTCPJSON_H
#define CONNECTIONTCPJSON_H

// #include <asio.hpp>

// compile without
#include <memory> //enabled_shared_from_this
#include <thread>

#include <asio.hpp>


class ConnectionTcpJson : public std::enable_shared_from_this<ConnectionTcpJson>
{
    public:
        /** Default constructor */
        ConnectionTcpJson( asio::ip::tcp::socket&& socket );

        void start();
        /** Default destructor */
        ~ConnectionTcpJson();



    private:
        asio::ip::tcp::socket socket; // moved from the main server loop

        // first impl. try to be as safe as possible
        asio::streambuf ingoing; // should limit the sizes
        asio::streambuf outgoing;

        // follow basic handler signature
        void on_read_message(asio::error_code error, std::size_t bytes_transferred);
};

#endif // CONNECTIONTCPJSON_H
