#include <iostream>

#include "ConnectionTcpJson.h"

using namespace std;
using namespace std::placeholders;

ConnectionTcpJson::ConnectionTcpJson( asio::ip::tcp::socket&& socket )
    : socket( std::move(socket) )
{}

void ConnectionTcpJson::start()
{
    std::cout << "a new connection , we'll read the data" << std::endl;

    asio::async_read_until( socket, ingoing, '\n',
                // ok, shared from this() necessary !
                std::bind(&ConnectionTcpJson::on_read_message, this->shared_from_this(), _1, _2) );

                // implementation from server1.tcp with lambda it was ok
                // do the same, so it is a problem of this ??
                /*
                [self = shared_from_this()] (asio::error_code error, std::size_t bytes_transferred) {

                    self->on_read_message( error, bytes_transferred );

              });*/
}

void ConnectionTcpJson::on_read_message(asio::error_code error, std::size_t bytes_transferred)
{
    cout << "size of message received: " << bytes_transferred << endl;
    //cout << "error_code: " << error_code.value() << endl;

    // get string
    std::string str_message( (std::istreambuf_iterator<char>(&ingoing)),
                              std::istreambuf_iterator<char>() );

    // some logic, json stuffs..call Businesss

    // make an echo, replace the str_message into output
    std::iostream os(&outgoing);
    //std::string message();
    os << str_message; // << add content, END ?

    // call again async
    //socket.async_send
    asio::async_write( socket, outgoing,
            //std::bind(&ConnectionTcpJson::on_write_message, self, _1, _2));
            [self = shared_from_this()] (asio::error_code error, std::size_t bytes_transferred) {

                cout << "The message has been send, bytes: " << bytes_transferred << endl;
            });
}


ConnectionTcpJson::~ConnectionTcpJson()
{
    cout << "Destructor ConnectionJsonTcp" << endl;
}
