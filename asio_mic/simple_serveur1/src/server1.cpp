#include <iostream>
#include <memory>
#include <optional>
#include <asio.hpp>

#include <thread>

using namespace std::placeholders;

class connection_t : public std::enable_shared_from_this<connection_t> {

    public:
        connection_t( asio::ip::tcp::socket&& socket)
            : socket( std::move(socket))
            {}

        ~connection_t() { std::cout << "Destructor connection_t" << std::endl; }
        // on coonection, called by async,
        // can continue to call async to fill io_context to extend possibility
        void start() {
            std::cout << "a new connection , we'll read the data" << std::endl;

            asio::async_read_until( socket, streambuf, '\n',
              [self = shared_from_this()] (asio::error_code error, std::size_t bytes_transferred) {

                   std::cout << "received: " << std::istream(&self->streambuf).rdbuf();

                   // https://stackoverflow.com/questions/44904295/convert-stdstring-to-boostasiostreambuf

                   //asio::streambuf sbuf;
                   std::iostream os(&self->bufEcho);
                   std::string message("ECHO\n");
                   os << message;


                   // make echo
                   //asio::async_write(self->socket, sbuf, //asio::streambuf("ECHO"),
                   //                  std::bind(&connection_t::send_response, self, _1, _2));
                   self->socket.async_send( asio::buffer("ECHO\n"), //self->bufEcho, //
                                            std::bind(&connection_t::send_response, self, _1, _2));

                    std::cout << "after async_write" << std::endl;
                   // try ...  but certainly other error, working if commented or not good
                   //std::this_thread::sleep_for(std::chrono::seconds(3));
                   std::cout << "\nafter pause" << std::endl;
              });
        }

        void send_response( const asio::error_code& error, std::size_t bytes_transferred ) {
            std::cout << " work is done from server point of view : " << bytes_transferred << std::endl;
        }

    private:
        asio::ip::tcp::socket socket; // moved from the main server loop
        asio::streambuf streambuf; // internal buffer, basic usage here
        //asio::streambuf streambuf;
        //asio::buffer bufEcho("ECHO\n");
        asio::streambuf bufEcho; //("ECHO\n");
};

class Server {

    public :
        Server( asio::io_context& io, std::uint_least16_t port ) //unsigned int port ) //std::uint_least16_t port )
            : io_context (io),
               // construct and opens it
              acceptor( io, asio::ip::tcp::endpoint( asio::ip::tcp::v4(), port) )
            {}

        // start the server in running this loop
        // may split start / async_accept
        void async_accept() {

            std::cout << "Server async_accept, new socket " << std::endl;

            // member in tuto, but can create on stack, then moved ?
            // asio::ip::tcp::socket accept_socket(io_context); /* constructor with context */
            accept_socket.emplace( io_context );

            // on received message, run a new connection, why shared ?
            acceptor.async_accept( *accept_socket,
                                   [&]( asio::error_code error) {
                                        std::make_shared<connection_t>(std::move(*accept_socket))->start();
                                        // call recursive, io_context::run() always busy
                                        async_accept();
                                  });
        }

    private :
       asio::io_context& io_context;
       asio::ip::tcp::acceptor acceptor; /*accept new  socket connections*/
       // tuto optional <socket>... ??
       //asio::ip::tcp::socket accept_socket; /* socket...a file handler for network */
       std::optional<asio::ip::tcp::socket> accept_socket;
};

int main() {
    // basic async, one unique io_context, one thread
    asio::io_context io;
    std::cout << "entry " << std::endl;
    Server server( io, 12345 );
    server.async_accept(); // return directly, filled io with work to do
    std::cout << "main: server running " << std::endl;
    io.run(); // blocks here
    std::cout << "end " << std::endl;
    return 0;
}
