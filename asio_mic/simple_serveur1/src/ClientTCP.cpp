#include <iostream>
#include <asio.hpp>

using namespace std::placeholders;

asio::io_context io;
asio::ip::tcp::socket socket_client(io);

asio::streambuf response; // it was the big mistake !! must live, declared inside on_write did not work

// to not forget const error_code& !
void on_write( const asio::error_code &error, std::size_t bytes_transferred ) {



    std::cout << "on_write nb bytes " << bytes_transferred  << std::endl;
    //socket_client.async
    /*
    asio::async_read( socket_client, response,
                     [&]( const asio::error_code &error, std::size_t bytes_transferred ) {
                        std::cout << "handler of async_read " << bytes_transferred << std::endl;
                        std::cout << "response :  " << std::endl;
                     });
    */
    asio::async_read_until( socket_client, response, '\n',
                     [=]( const asio::error_code &error, std::size_t bytes_transferred ) {
                        std::cout << "handler of async_read " << bytes_transferred << std::endl;
                        std::cout << "response :  " << std::istream( &response ).rdbuf();
                     });
}

int main(int argc, char *argv[]) {

    asio::error_code error;

    //asio::io_context io;

    asio::ip::address address = asio::ip::make_address("127.0.0.1"); /* deal with both ipv4 and 6 */
    asio::ip::tcp::endpoint endpoint(address, 12345 );

    // could combine ?
    //asio::ip::tcp::socket socket_client(io);
    //socket_client( io );
    socket_client.connect(endpoint, error);

    if( ! error ) {
            std::cout << "connection ok, will send async data" << std::endl;
            // write data
            socket_client.async_send( asio::buffer("toto\n"),  //send operation may not transmit all data, consider async_write
                                      //std::bind(&on_write, _1, _2) );    // signature void handler(error_code&, std::size_t bytes_transferred
                                      &on_write ); // signature void handler( CONST error_code&, std::size_t bytes_transferred)

    } else {
        std::cout << "Error connection to server" << std::endl;
    }

    std::cout << "before io.run()" << std::endl;
    io.run(); //if forget this, programm send data but stop after, no handler
    return 0;
}
