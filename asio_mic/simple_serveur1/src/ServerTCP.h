#ifndef SERVERTCP_H
#define SERVERTCP_H

#include <asio.hpp>
#include <optional>

// compile without, certainly not necessary
#include <memory>


/** Server part
    May contain a link to the Business Model, or be a part of it
    Here test only fonctionnalities first
*/
class ServerTCP
{
    public:
        /** Default constructor */
        ServerTCP( std::uint_least16_t port, asio::io_context& io_context);
        //ServerTCP( std::uint_least16_t port);


        void start(); //startServer
/*
        // wanted interface
        // would  like to keep logic in Server, this shoud be a completion handler
        onMessageReveiced() {
            // std::future = getJson()

            // call business synchronously  (at first)
            // Json answer  = egideBusiness->requestFromClient( jsonMessage )

            // async call to
            // ConnectionTcpJson . answerToClient()
        }
*/

        bool isRunning() const {return false;}
        /** Default destructor */
        virtual ~ServerTCP();

    private:
        asio::io_context& io_server_context;
        asio::ip::tcp::acceptor acceptor;
        std::optional<asio::ip::tcp::socket> accept_socket;

        // loopListening, called at start
        void loopListening();
};

#endif // SERVERTCP_H
