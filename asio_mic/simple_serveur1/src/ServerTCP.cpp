#include <iostream>

#include "ServerTCP.h"
#include "ConnectionTcpJson.h"

using namespace std;

using tcp = asio::ip::tcp;
using error_code = asio::error_code;




ServerTCP::ServerTCP( uint_least16_t port, asio::io_context& io_context)
      :  io_server_context( io_context ),
         acceptor( io_context, tcp::endpoint( asio::ip::tcp::v4(), port))
{}

/*
ServerTCP::ServerTCP( std::uint_least16_t port )
      :  io_server_context( asio::io_context() ),
         acceptor( io_server_context, tcp::endpoint( asio::ip::tcp::v4(), port) )
{}
*/
void ServerTCP::start()
{
    cout << "start server " << endl;
    loopListening();
}

void ServerTCP::loopListening()
{
    // fill io_context with a first work, then always creates  a new one
    accept_socket.emplace( io_server_context );

    acceptor.async_accept( *accept_socket,
                           [&]( asio::error_code error) {
                                std::make_shared<ConnectionTcpJson>( std::move(*accept_socket))->start();
                                // call recursive, io_context::run() always busy
                                loopListening();
                             });

}

ServerTCP::~ServerTCP()
{
    cout << "Destructoor Server" << endl;
}

