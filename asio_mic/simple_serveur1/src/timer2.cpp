#include <iostream>
#include <asio.hpp>

/** Version asynchrone */


/** async action in a callback function, will be printed after the async wait finishes
    signature must be void handler( const error_code& )
*/
void print(const asio::error_code& /*e */)
{
    std::cout << "Hello World" << std::endl;
}

int main(int argc, char *argv[])
{
    // need at least on I/O execution context, must be passed to all core asio classes that provide I/O functionality  as first argument
    asio::io_context io;

    asio::steady_timer timer( io, asio::chrono::seconds(5) );
    std::cout << "creation timer, execute" << std::endl;

    // execute timer synch
    // timer.wait();
    // execute timer async
    timer.async_wait( &print );
    std::cout << "after async_wait, io.run()" << std::endl;

    // must be invoked for the handler to be executed
    // some work must be added before calling io.run()
    io.run();

    std::cout << "after io.run()" << std::endl;
	return 0;
}
